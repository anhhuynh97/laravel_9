<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //  two migrations were created with the same name because migrations won't have class names anymore 
    //  2014_10_12_000000_create_users_table.php
    //  2022_04_19_000000_create_users_table.php
    // có thể migrate bổ sung column thêm vào table mà ko cần tạo lại table 
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('sex')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('sex');
        });    }
};
