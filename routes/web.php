<?php

use Illuminate\Support\Facades\Route;
use App\Enums\Category;
use App\Enums\UserRole;
use App\Http\Controllers\UserController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $user = new User();
    // $user->role = 'jkhkh';  báo lỗi vì ko phải kiểu enum UserRole đã được cast (viết trong model User)
    // phải là :
    $user->role = UserRole::VISITOR;
    $user->name = "Huy";
    $user->email = "huy1234@gmail.com";
    $user->password = "abc123";

    $user->save();
    return 'Done'

;
});
// Implicit Enum Binding , nó sẽ tìm trong enum Category có value nào giống {category} mà mình nhập vào , nếu có thì chạy code bên dưới return 'VERY TRUE' , ko thì 404
// Implicit Enum Binding giảm bớt việc viết đi viết lại nhiều routes giống nhau về cấu trúc 
Route::get('/categories/{category}', function (Category $category) {
    return 'VERY TRUE';
});

Route::match(['get', 'post'],'/users/edit/{id}', [UserController::class, 'edit'])->name('user.edit');

