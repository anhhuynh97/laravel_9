<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    public function edit($id,UserRequest $request){
        $user = User::find($id);
        if ($request->isMethod('post')) {
            $post_data = $request->except('_token');
            $result = $user->update($post_data);
            if ($result) {
                session()->flash('success','updated');
            } else {
                session()->flash('error', 'fail');
                return redirect()->back()->with($post_data);
            }
            return redirect(route('user.edit',$id));   
             }
        return view('userList',compact('user'));
    }
}
